package com.aswdc.matrimony.model;

import java.io.Serializable;

public class CityModel implements Serializable {
    int cityId;
    String cityName;

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String toString(){
        return "CityModel{" +
                "CityId=" + cityId +
                ", CityName='" + cityName + '\'' +
                '}';
    }
}
