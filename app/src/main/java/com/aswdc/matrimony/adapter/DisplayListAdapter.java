package com.aswdc.matrimony.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.fragment.DisplayListFragment;
import com.aswdc.matrimony.model.UserModel;
import com.aswdc.matrimony.util.Constant;
import com.aswdc.matrimony.util.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DisplayListAdapter extends RecyclerView.Adapter<DisplayListAdapter.UserHolder> {

    Context context;
    ArrayList<UserModel> userList;
    OnViewClickListener onViewClickListener;

    public DisplayListAdapter(Context context, ArrayList<UserModel> userList , OnViewClickListener onViewClickListener) {
        this.context = context;
        this.userList = userList;
        this.onViewClickListener = onViewClickListener;
    }

    public interface OnViewClickListener{
         void onDeleteClick(int position);
         void onItemClick(int position);
         void onFavouriteClick(int position);
    }

    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_display_list, null));
    }



    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
//        final Calendar calendar = Calendar.getInstance();
//        Date date = Utils.getDateFromString(userList.get(position).getDateOfBirth());
//        calendar.setTime(date);
//        int year = Calendar.YEAR;
//        Log.d("Year:::", year+" ");
//        int month = Calendar.MONTH;
//        Log.d("Month:::",month + " ");
//        int day = Calendar.DAY_OF_MONTH;
//        Log.d("Day:::",day+ "");
        holder.tvFullName.setText(userList.get(position).getName() + " " + userList.get(position).getFatherName() + " " + userList.get(position).getSurname());
        holder.tvDob.setText( userList.get(position).getDateOfBirth() + " | " + userList.get(position).getCityName());
        holder.ivFavouriteUser.setImageResource(userList.get(position).getIsFavourite() == 0 ? R.drawable.not_favorite_user : R.drawable.favourite_user);

        holder.ivDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onViewClickListener != null) {
                    onViewClickListener.onDeleteClick(position);
                }
            }
        });

        holder.itemView.setOnClickListener(view -> {
            if(onViewClickListener != null){
                onViewClickListener.onItemClick(position);
            }
        });

        holder.ivFavouriteUser.setOnClickListener(view -> {
            if(onViewClickListener != null){
                onViewClickListener.onFavouriteClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFullName)
        TextView tvFullName;
        @BindView(R.id.tvDob)
        TextView tvDob;
        @BindView(R.id.tvCity)
        TextView tvCity;
        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavouriteUser)
        ImageView ivFavouriteUser;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class GenderViewPagerAdapter extends FragmentStatePagerAdapter {

        private String tabTitles[] = new String[]{"Male", "Female"};
        private Context context;

        public GenderViewPagerAdapter(@NonNull FragmentManager fm , Context context) {
            super(fm);
            this.context = context;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return DisplayListFragment.getInstanceGender(Constant.MALE);
            }
            else{
                return DisplayListFragment.getInstanceGender(Constant.FEMALE);
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }

}

