package com.aswdc.matrimony.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.Spinner;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.adapter.CityAdapter;
import com.aswdc.matrimony.database.City;
import com.aswdc.matrimony.database.User;
import com.aswdc.matrimony.model.CityModel;
import com.aswdc.matrimony.model.UserModel;
import com.aswdc.matrimony.util.Constant;
import com.aswdc.matrimony.util.Utils;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends BaseActivity {

    @BindView(R.id.etActName)
    TextInputEditText etActName;
    @BindView(R.id.etActFatherName)
    TextInputEditText etActFatherName;
    @BindView(R.id.etActSurame)
    TextInputEditText etActSurame;
    @BindView(R.id.rbActMale)
    MaterialRadioButton rbActMale;
    @BindView(R.id.rbActFemale)
    MaterialRadioButton rbActFemale;
    @BindView(R.id.etActDateOfBirth)
    TextInputEditText etActDateOfBirth;
    @BindView(R.id.etActPhoneNumber)
    TextInputEditText etActPhoneNumber;
    @BindView(R.id.etActEmail)
    TextInputEditText etActEmail;
    @BindView(R.id.btnActRegister)
    Button btnActRegister;
    @BindView(R.id.spActCity)
    Spinner spCity;

    @BindView(R.id.chbCricket)
    CheckBox chbCricket;
    @BindView(R.id.chbFootball)
    CheckBox chbFootball;
    @BindView(R.id.chbHockey)
    CheckBox chbHockey;
    @BindView(R.id.chbBadminton)
    CheckBox chbBadminton;
    @BindView(R.id.chbDancing)
    CheckBox chbDancing;
    @BindView(R.id.chbPainting)
    CheckBox chbPainting;


    @BindView(R.id.chbEnglish)
    CheckBox chbEnglish;
    @BindView(R.id.chbGujarati)
    CheckBox chbGujarati;
    @BindView(R.id.chbHindi)
    CheckBox chbHindi;
    @BindView(R.id.chbMarathi)
    CheckBox chbMarathi;


    String startingDate = "1990-08-10";

    CityAdapter cityAdapter;
    ArrayList<CityModel> cityList = new ArrayList<>();

    UserModel userModel;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        setupActionBar(getString(R.string.add_user_title), true);
        setDataToView();
        getDataForUpdate();
    }

    void getDataForUpdate(){
        if(getIntent().hasExtra(Constant.USER_OBJECT)){
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            getSupportActionBar().setTitle(R.string.title_edit_user_details);
            etActName.setText(userModel.getName());
            etActFatherName.setText(userModel.getFatherName());
            etActSurame.setText(userModel.getSurname());

            etActPhoneNumber.setText(userModel.getPhoneNumber());
            etActDateOfBirth.setText(userModel.getDateOfBirth());
            if (userModel.getGender() == Constant.MALE) {
                rbActMale.setChecked(true);
            } else {
                rbActFemale.setChecked(true);
            }
            etActEmail.setText(userModel.getEmail());
            spCity.setSelection(getSelectedPositionFromCityId(userModel.getCityId()));


            //set data for languages
            if(userModel.getLanguage().contains(chbEnglish.getText().toString())){
                chbEnglish.setChecked(true);
            }
            if(userModel.getLanguage().contains(chbHindi.getText().toString())){
                chbHindi.setChecked(true);
            }
            if(userModel.getLanguage().contains(chbGujarati.getText().toString())){
                chbGujarati.setChecked(true);
            }
            if(userModel.getLanguage().contains(chbMarathi.getText().toString())){
                chbMarathi.setChecked(true);
            }

            //set data for hobbies
            if(userModel.getHobbies().contains(chbCricket.getText().toString())){
                chbCricket.setChecked(true);
            }
            if(userModel.getHobbies().contains(chbFootball.getText().toString())){
                chbFootball.setChecked(true);
            }
            if(userModel.getHobbies().contains(chbHockey.getText().toString())){
                chbHockey.setChecked(true);
            }
            if(userModel.getHobbies().contains(chbBadminton.getText().toString())){
                chbBadminton.setChecked(true);
            }
            if(userModel.getHobbies().contains(chbDancing.getText().toString())){
                chbDancing.setChecked(true);
            }
            if(userModel.getHobbies().contains(chbPainting.getText().toString())){
                chbPainting.setChecked(true);
            }

        }
    }

    int getSelectedPositionFromCityId(int cityId) {
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getCityId() == cityId) {
                return i;
            }
        }
        return 0;
    }

    void SetSpinnerData() {
        cityList.addAll(new City(this).getCityList());
        cityAdapter = new CityAdapter(this, cityList);
        spCity.setAdapter(cityAdapter);
    }

    void setDataToView() {
        final Calendar newCalendar = Calendar.getInstance();
        etActDateOfBirth.setText(
                String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                        String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" +
                        String.format("%02d", newCalendar.get(Calendar.YEAR)));

       SetSpinnerData();
    }

    @OnClick(R.id.etActDateOfBirth)
    public void onEtActDateOfBirthClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = Utils.getDateFromString(startingDate);
        newCalendar.setTimeInMillis(date.getTime());
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                RegistrationActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                etActDateOfBirth.setText(String.format("%02d", day) + "/" + String.format("%02d", (month + 1)) + "/" + year);
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    String getLanguages(){
        String languageList = "";
        if (chbEnglish.isChecked()) {
            languageList += "," + chbEnglish.getText().toString();
        }
        if (chbHindi.isChecked()) {
            languageList += "," + chbHindi.getText().toString();
        }
        if (chbGujarati.isChecked()) {
            languageList += "," + chbGujarati.getText().toString();
        }
        if (chbMarathi.isChecked()) {
            languageList += "," + chbMarathi.getText().toString();
        }

        return languageList.substring(1);
    }

    String getHobbies(){
        String hobbiesList = "";
        if (chbCricket.isChecked()) {
            hobbiesList += "," + chbCricket.getText().toString();
        }
        if (chbBadminton.isChecked()) {
            hobbiesList += "," + chbBadminton.getText().toString();
        }
        if (chbDancing.isChecked()) {
            hobbiesList += "," + chbDancing.getText().toString();
        }
        if (chbHockey.isChecked()) {
            hobbiesList += "," + chbHockey.getText().toString();
        }
        if (chbPainting.isChecked()) {
            hobbiesList += "," + chbPainting.getText().toString();
        }
        if (chbFootball.isChecked()) {
            hobbiesList += "," + chbFootball.getText().toString();
        }
        return hobbiesList.substring(1);
    }

    @OnClick(R.id.btnActRegister)
    public void onBtnActRegisterClicked() {
        if(isValidUser()) {
            if(userModel == null) {
                long lastInsertedId = new User(getApplicationContext()).insertUser(etActName.getText().toString(),
                        etActFatherName.getText().toString(), etActSurame.getText().toString(), rbActMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                        Utils.getFormatedDateToInsert(etActDateOfBirth.getText().toString()), getHobbies() ,
                        etActPhoneNumber.getText().toString(), etActEmail.getText().toString(), getLanguages() ,
                        cityList.get(spCity.getSelectedItemPosition()).getCityId(),0);
                showToast(lastInsertedId > 0 ? "User Added Successfully" : "Something went wrong");
            }
            else{
                long lastInsertedId = new User(getApplicationContext()).updateUserByUserId(etActName.getText().toString(),
                        etActFatherName.getText().toString(), etActSurame.getText().toString(), rbActMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                        Utils.getFormatedDateToInsert(etActDateOfBirth.getText().toString()), getHobbies() ,
                        etActPhoneNumber.getText().toString(), etActEmail.getText().toString(), getLanguages(),
                        cityList.get(spCity.getSelectedItemPosition()).getCityId(),userModel.getIsFavourite(),userModel.getUserId());
                showToast(lastInsertedId > 0 ? "User Updated Successfully" : "Something went wrong");
            }
            Intent intent = new Intent(RegistrationActivity.this,GenderWiseUserListActivity.class);
            startActivity(intent);
            finish();
        }
    }
    boolean isValidUser() {

        //name validation
        boolean isValid = true;
        if (TextUtils.isEmpty(etActName.getText().toString())) {
            isValid = false;
            etActName.setError(getString(R.string.err_enter_name));
        }
        if (TextUtils.isEmpty(etActFatherName.getText().toString())) {
            isValid = false;
            etActFatherName.setError(getString(R.string.err_enter_father_name));
        }
        if (TextUtils.isEmpty(etActSurame.getText().toString())) {
            isValid = false;
            etActSurame.setError(getString(R.string.err_enter_surname));
        }

        //phone number validation
        if (TextUtils.isEmpty(etActPhoneNumber.getText().toString())) {
            isValid = false;
            etActPhoneNumber.setError(getString(R.string.err_enter_phone));
        } else if (etActPhoneNumber.getText().toString().length() < 10) {
            isValid = false;
            etActPhoneNumber.setError(getString(R.string.err_enter_valid_phone));
        }

        //email validation
        if (TextUtils.isEmpty(etActEmail.getText().toString())) {
            isValid = false;
            etActEmail.setError(getString(R.string.err_enter_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etActEmail.getText().toString()).matches()) {
            isValid = false;
            etActEmail.setError(getString(R.string.err_enter_valid_email));
        }

        //city validation
        if (spCity.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.err_select_city));
        }

        //language validation
        if(!chbEnglish.isChecked()){
            if(!chbHindi.isChecked()){
                if(!chbGujarati.isChecked()){
                    if(!chbMarathi.isChecked()){
                        showToast(getString(R.string.err_select_one_language));
                        isValid = false;
                    }
                }
            }
        }

        if(!chbCricket.isChecked()){
            if(!chbFootball.isChecked()){
                if(!chbHockey.isChecked()){
                    if(!chbDancing.isChecked()){
                        if(!chbPainting.isChecked()){
                            if(!chbBadminton.isChecked()){
                                showToast(getString(R.string.err_select_one_hobby));
                                isValid = false;
                            }
                        }
                    }
                }
            }
        }
        return isValid;
    }
}

