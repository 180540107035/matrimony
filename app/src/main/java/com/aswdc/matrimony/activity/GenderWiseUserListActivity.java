package com.aswdc.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.adapter.DisplayListAdapter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenderWiseUserListActivity extends BaseActivity {
    @BindView(R.id.tlGenders)
    TabLayout tlGenders;
    @BindView(R.id.vpUserList)
    ViewPager vpUserList;
    DisplayListAdapter.GenderViewPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender_wise_list);
        ButterKnife.bind(this);
        setupActionBar(getString(R.string.title_user_list),true);
        setUpViewPagerAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favourite_user,menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(vpUserList!=null) {
            setUpViewPagerAdapter();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.ivFavouriteUser){
            Intent intent = new Intent(this, FavouriteUserActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    void setUpViewPagerAdapter(){
        adapter = new DisplayListAdapter.GenderViewPagerAdapter(getSupportFragmentManager(),this);
        vpUserList.setAdapter(adapter);
        tlGenders.setupWithViewPager(vpUserList);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(GenderWiseUserListActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
